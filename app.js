function xo(str) {
    strarr = str.split('');

    let countx = 0;
    for (let i = 0; i < strarr.length; i++) {
        if (strarr[i] === 'x') {
            countx++;
            continue;
        };

    }
    let counto = 0;
    for (let i = 0; i < strarr.length; i++) {
        if (strarr[i] === 'o') {
            counto++;
            continue;
        }
    };

    if (countx == counto) {
        return true;
    } else
        return false;

}
}

// TEST CASES
console.log(xo('xoxoxo')); // true
console.log(xo('oxooxo')); // false
console.log(xo('oxo')); // false
console.log(xo('xxxooo')); // true
console.log(xo('xoxooxxo')); // true